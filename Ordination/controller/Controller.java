package controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.Patient;
import ordination.Pn;
import storage.Storage;

public class Controller {
	private Storage storage;
	private static Controller controller;

	private Controller() {
		storage = new Storage();
	}

	public static Controller getController() {
		if (controller == null) {
			controller = new Controller();
		}
		return controller;
	}

	public static Controller getTestController() {
		return new Controller();
	}

	/**
	 * Hvis startDato er efter slutDato kastes en IllegalArgumentException og
	 * ordinationen oprettes ikke Pre: startDen, slutDen, patient og laegemiddel er
	 * ikke null
	 *
	 * @return opretter og returnerer en PN ordination.
	 */
	public Pn opretPNOrdination(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
			double antal) {
		checkStartFoerSlut(startDen, slutDen);
		if (antal <= 0) {
			throw new IllegalArgumentException("Antal skal være større end 0");
		}
		Pn pn = new Pn(patient, startDen, slutDen, antal);
		pn.setLaegemiddel(laegemiddel);

		return pn;
	}

	/**
	 * Opretter og returnerer en DagligFast ordination. Hvis startDato er efter
	 * slutDato kastes en IllegalArgumentException og ordinationen oprettes ikke
	 * Pre: startDen, slutDen, patient og laegemiddel er ikke null
	 */
	public DagligFast opretDagligFastOrdination(LocalDate startDen, LocalDate slutDen, Patient patient,
			Laegemiddel laegemiddel, double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {

		checkStartFoerSlut(startDen, slutDen); // throws IllegalArgumentException.

		if (morgenAntal <= 0 && middagAntal <= 0 && aftenAntal <= 0 && natAntal <= 0) {
			throw new IllegalArgumentException("Mindst én af antallene skal være større end 0");
		}
		DagligFast fast = new DagligFast(patient, startDen, slutDen);
		fast.setLaegemiddel(laegemiddel);

		fast.opretDosis(LocalTime.of(06, 00), morgenAntal);
		fast.opretDosis(LocalTime.of(12, 00), middagAntal);
		fast.opretDosis(LocalTime.of(18, 00), aftenAntal);
		fast.opretDosis(LocalTime.of(00, 00), natAntal);

		return fast;
	}

	/**
	 * Opretter og returnerer en DagligSkæv ordination. Hvis startDato er efter
	 * slutDato kastes en IllegalArgumentException og ordinationen oprettes ikke.
	 * Hvis antallet af elementer i klokkeSlet og antalEnheder er forskellige kastes
	 * også en IllegalArgumentException.
	 *
	 * Pre: startDen, slutDen, patient og laegemiddel er ikke null
	 */
	public DagligSkaev opretDagligSkaevOrdination(LocalDate startDen, LocalDate slutDen, Patient patient,
			Laegemiddel laegemiddel, LocalTime[] klokkeSlet, double[] antalEnheder) {

		checkStartFoerSlut(slutDen, slutDen);

		if (klokkeSlet.length != antalEnheder.length) {
			throw new IllegalArgumentException("Antallet af klokkeslet og antallet af enheder er forskellige.");
		}

		DagligSkaev skaev = new DagligSkaev(patient, startDen, slutDen);
		skaev.setLaegemiddel(laegemiddel);

		for (int i = 0; i < antalEnheder.length; i++) {
			skaev.opretDosis(klokkeSlet[i], antalEnheder[i]);
		}

		return skaev;
	}

	/**
	 * En dato for hvornår ordinationen anvendes tilføjes ordinationen. Hvis datoen
	 * ikke er indenfor ordinationens gyldighedsperiode kastes en
	 * IllegalArgumentException Pre: ordination og dato er ikke null
	 */
	public boolean ordinationPNAnvendt(Pn ordination, LocalDate dato) {
		boolean gyldig = ordination.givDosis(dato);
		if (!gyldig) {
			throw new IllegalArgumentException("Dato ikke indenfor periode.");
		}
		return gyldig;
	}

	/**
	 * Den anbefalede dosis for den pågældende patient (der skal tages hensyn til
	 * patientens vægt). Det er en forskellig enheds faktor der skal anvendes, og
	 * den er afhængig af patientens vægt. Pre: patient og lægemiddel er ikke null
	 */
	public double anbefaletDosisPrDoegn(Patient patient, Laegemiddel laegemiddel) {
		double vægt = patient.getVaegt();
		double enhed = 0;
		if (vægt < 25 && vægt > 0) {
			enhed = laegemiddel.getEnhedPrKgPrDoegnLet();
		} else if (25 <= vægt && vægt <= 120) {
			enhed = laegemiddel.getEnhedPrKgPrDoegnNormal();
		} else
			enhed = laegemiddel.getEnhedPrKgPrDoegnTung();
		return enhed * vægt;
	}

	/**
	 * For et givent vægtinterval og et givent lægemiddel, hentes antallet af
	 * ordinationer. Pre: laegemiddel er ikke null
	 */
	public int antalOrdinationerPrVægtPrLægemiddel(double vægtStart, double vægtSlut, Laegemiddel laegemiddel) {
		int counter = 0;
		for (Patient patient : storage.getAllPatienter()) {
			if (vægtStart <= patient.getVaegt() && patient.getVaegt() <= vægtSlut) {
				for (Ordination ordination : patient.getOrdinationer()) {
					if (ordination.getLaegemiddel().equals(laegemiddel)) {
						counter++;
					}
				}
			}

		}

		return counter;
	}

	public List<Patient> getAllPatienter() {
		return storage.getAllPatienter();
	}

	public List<Laegemiddel> getAllLaegemidler() {
		return storage.getAllLaegemidler();
	}

	/**
	 * Metode der kan bruges til at checke at en startDato ligger før en slutDato.
	 *
	 * @return true hvis startDato er før slutDato, false ellers.
	 */
	private void checkStartFoerSlut(LocalDate startDato, LocalDate slutDato) {
		if (slutDato.compareTo(startDato) < 0) {
			throw new IllegalArgumentException("Startdato skal være før slutdato.");
		}
	}

	public Patient opretPatient(String cpr, String navn, double vaegt) {
		Patient p = new Patient(cpr, navn, vaegt);
		storage.addPatient(p);
		return p;
	}

	public Laegemiddel opretLaegemiddel(String navn, double enhedPrKgPrDoegnLet, double enhedPrKgPrDoegnNormal,
			double enhedPrKgPrDoegnTung, String enhed) {
		Laegemiddel lm = new Laegemiddel(navn, enhedPrKgPrDoegnLet, enhedPrKgPrDoegnNormal, enhedPrKgPrDoegnTung,
				enhed);
		storage.addLaegemiddel(lm);
		return lm;
	}

	public void createSomeObjects() {
		this.opretPatient("121256-0512", "Jane Jensen", 63.4);
		this.opretPatient("070985-1153", "Finn Madsen", 83.2);
		this.opretPatient("050972-1233", "Hans Jørgensen", 89.4);
		this.opretPatient("011064-1522", "Ulla Nielsen", 59.9);
		this.opretPatient("090149-2529", "Ib Hansen", 87.7);

		this.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		this.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		this.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		this.opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

		this.opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(1), 123);

		this.opretPNOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(0), 3);

		this.opretPNOrdination(LocalDate.of(2019, 1, 20), LocalDate.of(2019, 1, 25), storage.getAllPatienter().get(3),
				storage.getAllLaegemidler().get(2), 5);

		this.opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(1), 123);

		this.opretDagligFastOrdination(LocalDate.of(2019, 1, 10), LocalDate.of(2019, 1, 12),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(1), 2, -1, 1, -1);

		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] an = { 0.5, 1, 2.5, 3 };

		this.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(2), kl, an);
	}

}
