package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.SortedMap;
import java.util.TreeMap;

public class Pn extends Ordination {

	private double antalEnheder;
	private SortedMap<LocalDate, Integer> doser = new TreeMap<>();

	/**
	 * Patient, startDen, slutDen != null. antalEnheder > 0
	 * 
	 * @param patient
	 * @param startDen
	 * @param slutDen
	 * @param antalEnheder
	 */
	public Pn(Patient patient, LocalDate startDen, LocalDate slutDen, double antalEnheder) {
		super(patient, startDen, slutDen);
		this.antalEnheder = antalEnheder;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		boolean gyldig = false;
		if (!givesDen.isBefore(super.getStartDen()) && !givesDen.isAfter(super.getSlutDen())) {
			gyldig = true;
			if (doser.containsKey(givesDen))
				doser.put(givesDen, doser.get(givesDen) + 1);
			else
				doser.put(givesDen, 1);
		}
		return gyldig;
	}

	/**
	 * Antal hele dage mellem startdato og slutdato. Begge dage inklusive.
	 * 
	 * @return antal dage PN gælder for
	 */
	@Override
	public int antalDage() {
		if (doser.size() < 2) {
			return 1;
		}
		return (int) ChronoUnit.DAYS.between(doser.firstKey(), doser.lastKey()) + 1;
	}

	/**
	 * returnerer døgndosis. pre: doser.size() != null
	 * 
	 * @return
	 */
	@Override
	public double doegnDosis() {
		int gangeGivet = 0;
		for (Integer dosis : doser.values()) {
			gangeGivet += dosis;
		}
		return (gangeGivet * antalEnheder) / antalDage();
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * antalDage();
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		return doser.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return "PN";
	}

	public SortedMap<LocalDate, Integer> getDoser() {
		return new TreeMap<LocalDate, Integer>(doser);
	}

}
