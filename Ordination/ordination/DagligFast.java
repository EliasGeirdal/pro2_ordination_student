package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;

public class DagligFast extends Ordination {
	private Dosis[] doser = new Dosis[4];
	private int INDEX = 0;

	public DagligFast(Patient patient, LocalDate startDen, LocalDate slutDen) {
		super(patient, startDen, slutDen);
	}

	public void opretDosis(LocalTime tid, double antal) {
		if (INDEX > 3) {
			throw new IndexOutOfBoundsException("Alle doser er fyldte");
		}
		Dosis dosis = new Dosis(tid, antal);
		doser[INDEX] = dosis;
		INDEX++;
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * super.antalDage();
	}

	@Override
	public double doegnDosis() {
		double antal = 0;
		for (int i = 0; i < doser.length; i++) {
			if (doser[i] != null)
			antal += doser[i].getAntal();
		}
		return antal;
	}

	public Dosis[] getDoser() {
		return Arrays.copyOf(doser, doser.length);
	}

	@Override
	public String getType() {
		return "Daglig fast";
	}
}
