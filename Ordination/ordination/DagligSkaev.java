package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class DagligSkaev extends Ordination {
	private List<Dosis> doser = new ArrayList<>();

	public DagligSkaev(Patient patient, LocalDate startDen, LocalDate slutDen) {
		super(patient, startDen, slutDen);
	}

	public void opretDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		if (!this.doser.contains(dosis))
			this.doser.add(dosis);
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * super.antalDage();
	}

	@Override
	public double doegnDosis() {
		double antal = 0;
		for (Dosis dosis : doser) {
			antal += dosis.getAntal();
		}
		return antal;
	}

	public List<Dosis> getDoser() {
		return new ArrayList<Dosis>(doser);
	}

	@Override
	public String getType() {
		return "Daglig skaev";
	}
}
