package test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Patient;
import ordination.Pn;

public class ControllerTest {
	private Patient patient;
	private Laegemiddel laegemiddel1, laegemiddel2;
	private Controller controller;
	private LocalDate start;
	private LocalDate slut;

	@Before
	public void setUp() throws Exception {
		controller = Controller.getTestController();
		patient = controller.opretPatient("123456-7890", "Fornavn Efternavn", 60.00);
		laegemiddel1 = controller.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		laegemiddel2 = controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		start = LocalDate.of(2020, 02, 01);
		slut = LocalDate.of(2020, 02, 10);

	}

	@Test
	public void testOpretPNOrdination() {
		Pn pn = controller.opretPNOrdination(start, slut, patient, laegemiddel1, 2);

		assertEquals(LocalDate.of(2020, 02, 01), pn.getStartDen());
		assertEquals(LocalDate.of(2020, 02, 10), pn.getSlutDen());
		assertEquals(2, pn.getAntalEnheder(), 0.001);
	}

	@Test
	public void testOpretPNOrdinationException_StartFoerSlut() {
		try {
			LocalDate start = LocalDate.of(2020, 02, 10);
			LocalDate slut = LocalDate.of(2020, 02, 01);
			Pn pn = controller.opretPNOrdination(start, slut, patient, laegemiddel1, 2);
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Startdato skal være før slutdato.");
		}
	}

	@Test
	public void testOpretPNOrdinationException_antal() {
		try {
			Pn pn = controller.opretPNOrdination(start, slut, patient, laegemiddel1, 0);
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Antal skal være større end 0");
		}
	}

	@Test
	public void testOpretDagligFastOrdination_TC1() {
		DagligFast dagligFast = controller.opretDagligFastOrdination(start, slut, patient, laegemiddel1, 1, 2, 3, 0);
		assertEquals(1, dagligFast.getDoser()[0].getAntal(), 0.0001);
		assertEquals(2, dagligFast.getDoser()[1].getAntal(), 0.0001);
		assertEquals(3, dagligFast.getDoser()[2].getAntal(), 0.0001);
		assertEquals(0, dagligFast.getDoser()[3].getAntal(), 0.0001);
	}

	@Test
	public void testOpretDagligFastOrdination_TC2() {
		try {
			DagligFast dagligFast = controller.opretDagligFastOrdination(start, slut, patient, laegemiddel1, 0, 0, 0,
					0);
		} catch (IllegalArgumentException e) {
			assertEquals("Mindst én af antallene skal være større end 0", e.getMessage());
		}
	}

	@Test
	public void testOpretDagligFastOrdination_TC3() {
		try {
			LocalDate start = LocalDate.of(2020, 02, 10);
			LocalDate slut = LocalDate.of(2020, 02, 01);
			DagligFast dagligFast = controller.opretDagligFastOrdination(start, slut, patient, laegemiddel1, 1, 1, 0,
					0);
		} catch (IllegalArgumentException e) {
			assertEquals("Startdato skal være før slutdato.", e.getMessage());
		}

	}

	@Test
	public void testOpretDagligSkaevOrdination() {
		LocalDate start = LocalDate.of(2020, 10, 01);
		LocalDate slut = LocalDate.of(2020, 10, 10);
		LocalTime[] klokkeSlet = { LocalTime.of(12, 00), LocalTime.of(14, 00) };
		double[] antalEnheder = { 2, 4 };
		DagligSkaev ds = controller.opretDagligSkaevOrdination(start, slut, patient, laegemiddel1, klokkeSlet,
				antalEnheder);
		// tjek parameter:
		// startdato:
		assertEquals(LocalDate.of(2020, 10, 01), ds.getStartDen());
		// slutdato
		assertEquals(LocalDate.of(2020, 10, 10), ds.getSlutDen());

		// test om klokkeslet og antalEnheder stemmer overens med ds objektet.
		LocalTime[] dsKlokkeSlet = { ds.getDoser().get(0).getTid(), ds.getDoser().get(1).getTid() };
		double[] dsEnheder = { ds.getDoser().get(0).getAntal(), ds.getDoser().get(1).getAntal() };
		assertArrayEquals(klokkeSlet, dsKlokkeSlet);
		assertArrayEquals(antalEnheder, dsEnheder, 0.001);
	}

	@Test
	public void testOpretDagligSkaevOrdinationException_UligeAntalArrays() {
		try {
			LocalTime[] klokkeSlet = { LocalTime.of(19, 00), LocalTime.of(21, 00) };
			double[] antalEnheder = { 2 };
			DagligSkaev ds = controller.opretDagligSkaevOrdination(start, slut, patient, laegemiddel1, klokkeSlet,
					antalEnheder);

		} catch (IllegalArgumentException e) {
			assertEquals("Antallet af klokkeslet og antallet af enheder er forskellige.", e.getMessage());
		}
	}

	@Test
	public void testOpretDagligSkaevOrdinationException_StartFoerSlut() {
		try {
			LocalDate start = LocalDate.of(2020, 10, 10);
			LocalDate slut = LocalDate.of(2020, 10, 01);
			LocalTime[] klokkeSlet = { LocalTime.of(12, 00), LocalTime.of(14, 00) };
			double[] antalEnheder = { 2, 4 };
			DagligSkaev ds = controller.opretDagligSkaevOrdination(start, slut, patient, laegemiddel1, klokkeSlet,
					antalEnheder);
		} catch (IllegalArgumentException e) {
			assertEquals("Startdato skal være før slutdato.", e.getMessage());
		}
	}

	@Test
	public void testOrdinationPNAnvendt_TC1() {
		Pn pn = controller.opretPNOrdination(start, slut, patient, laegemiddel1, 2);
		assertEquals(true, controller.ordinationPNAnvendt(pn, LocalDate.of(2020, 02, 01)));
		assertEquals(true, controller.ordinationPNAnvendt(pn, LocalDate.of(2020, 02, 10)));
	}

	@Test
	public void testOrdinationPNAnvendtException_TC2() {
		try {
			Pn pn = controller.opretPNOrdination(start, slut, patient, laegemiddel1, 2);
			controller.ordinationPNAnvendt(pn, LocalDate.of(2020, 01, 31));
		} catch (IllegalArgumentException e) {
			assertEquals("Dato ikke indenfor periode.", e.getMessage());
		}
	}

	@Test
	public void testOrdinationPNAnvendtException_TC3() {
		try {
			Pn pn = controller.opretPNOrdination(start, slut, patient, laegemiddel1, 2);
			controller.ordinationPNAnvendt(pn, LocalDate.of(2020, 02, 11));
		} catch (IllegalArgumentException e) {
			assertEquals("Dato ikke indenfor periode.", e.getMessage());
		}
	}

	@Test
	public void testAnbefaletDosisPrDoegn_TC1() {
		Patient patient = new Patient("123456-7890", "Fornavn Efternavn", 0);
		assertEquals(0, controller.anbefaletDosisPrDoegn(patient, laegemiddel1), 0.001);
	}

	@Test
	public void testAnbefaletDosisPrDoegn_TC2() {
		Patient patient = new Patient("123456-7890", "Fornavn Efternavn", 24);
		assertEquals(2.4, controller.anbefaletDosisPrDoegn(patient, laegemiddel1), 0.001);
	}

	@Test
	public void testAnbefaletDosisPrDoegn_TC3() {
		Patient patient = new Patient("123456-7890", "Fornavn Efternavn", 25);
		assertEquals(3.75, controller.anbefaletDosisPrDoegn(patient, laegemiddel1), 0.001);
	}

	@Test
	public void testAnbefaletDosisPrDoegn_TC4() {
		Patient patient = new Patient("123456-7890", "Fornavn Efternavn", 120);
		assertEquals(18, controller.anbefaletDosisPrDoegn(patient, laegemiddel1), 0.001);
	}

	@Test
	public void testAnbefaletDosisPrDoegn_TC5() {
		Patient patient = new Patient("123456-7890", "Fornavn Efternavn", 121);
		assertEquals(19.36, controller.anbefaletDosisPrDoegn(patient, laegemiddel1), 0.001);
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel_TC1() {
		// p1 ("cpr", "navn", 60.00) med l1
		Patient p2 = controller.opretPatient("416038-5151", "Bay Amaliensen", 120.1);
		Patient p3 = controller.opretPatient("443322-1100", "James Benjaminsdóttir", 120.0);
		Patient p4 = controller.opretPatient("443322-1100", "navn", 23);
		Patient p5 = controller.opretPatient("443322-1100", "navn", 22);

		Pn p21_pn1 = controller.opretPNOrdination(start, slut, patient, laegemiddel1, 2);
		Pn p2_pn1 = controller.opretPNOrdination(start, slut, p2, laegemiddel1, 2);
		Pn p2_pn2 = controller.opretPNOrdination(start, slut, p2, laegemiddel2, 2);
		Pn p3_pn1 = controller.opretPNOrdination(start, slut, p3, laegemiddel1, 2);
		Pn p4_pn1 = controller.opretPNOrdination(start, slut, p4, laegemiddel2, 2);
		Pn p5_pn1 = controller.opretPNOrdination(start, slut, p5, laegemiddel1, 2);
		assertEquals(1, controller.antalOrdinationerPrVægtPrLægemiddel(50, 70, laegemiddel1));
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel_TC2() {
		// p1 ("cpr", "navn", 60.00) med l1
		Patient p2 = controller.opretPatient("416038-5151", "Bay Amaliensen", 120.1);
		Patient p3 = controller.opretPatient("443322-1100", "James Benjaminsdóttir", 120.0);
		Patient p4 = controller.opretPatient("443322-1100", "navn", 23);
		Patient p5 = controller.opretPatient("443322-1100", "navn", 22);

		Pn p21_pn1 = controller.opretPNOrdination(start, slut, patient, laegemiddel1, 2);
		Pn p2_pn1 = controller.opretPNOrdination(start, slut, p2, laegemiddel1, 2);
		Pn p2_pn2 = controller.opretPNOrdination(start, slut, p2, laegemiddel2, 2);
		Pn p3_pn1 = controller.opretPNOrdination(start, slut, p3, laegemiddel1, 2);
		Pn p4_pn1 = controller.opretPNOrdination(start, slut, p4, laegemiddel2, 2);
		Pn p5_pn1 = controller.opretPNOrdination(start, slut, p5, laegemiddel1, 2);
		assertEquals(4, controller.antalOrdinationerPrVægtPrLægemiddel(22, 123, laegemiddel1));
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel__TC3() {
		// p1 ("cpr", "navn", 60.00) med l1
		Patient p2 = controller.opretPatient("416038-5151", "Bay Amaliensen", 120.1);
		Patient p3 = controller.opretPatient("443322-1100", "James Benjaminsdóttir", 120.0);
		Patient p4 = controller.opretPatient("443322-1100", "navn", 23);
		Patient p5 = controller.opretPatient("443322-1100", "navn", 22);

		Pn p2_pn1 = controller.opretPNOrdination(start, slut, p2, laegemiddel1, 2);
		Pn p2_pn2 = controller.opretPNOrdination(start, slut, p2, laegemiddel2, 2);
		Pn p3_pn1 = controller.opretPNOrdination(start, slut, p3, laegemiddel1, 2);
		Pn p4_pn1 = controller.opretPNOrdination(start, slut, p4, laegemiddel2, 2);
		Pn p5_pn1 = controller.opretPNOrdination(start, slut, p5, laegemiddel1, 2);
		assertEquals(1, controller.antalOrdinationerPrVægtPrLægemiddel(22, 71, laegemiddel2));
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel__TC4() {
		// p1 ("cpr", "navn", 60.00) med l1
		Patient p2 = controller.opretPatient("416038-5151", "Bay Amaliensen", 120.1);
		Patient p3 = controller.opretPatient("443322-1100", "James Benjaminsdóttir", 120.0);
		Patient p4 = controller.opretPatient("443322-1100", "navn", 23);
		Patient p5 = controller.opretPatient("443322-1100", "navn", 22);

		Pn p2_pn1 = controller.opretPNOrdination(start, slut, p2, laegemiddel1, 2);
		Pn p2_pn2 = controller.opretPNOrdination(start, slut, p2, laegemiddel2, 2);
		Pn p3_pn1 = controller.opretPNOrdination(start, slut, p3, laegemiddel1, 2);
		Pn p4_pn1 = controller.opretPNOrdination(start, slut, p4, laegemiddel2, 2);
		Pn p5_pn1 = controller.opretPNOrdination(start, slut, p5, laegemiddel1, 2);
		assertEquals(2, controller.antalOrdinationerPrVægtPrLægemiddel(120, 130, laegemiddel1));
	}

}
