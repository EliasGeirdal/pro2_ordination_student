package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.Patient;
import ordination.Pn;

public class PnTest {
	Patient patient;

	@Before
	public void setUp() throws Exception {
		patient = new Patient("123456-7890", "Fornavn Efternavn", 60.00);
	}

	/**
	 * Pre: Patient, startDen, slutDen != null. antalEnheder > 0. Redundant to test
	 * constructor as it is implicitly tested in the other methods.
	 * 
	 */
	@Test
	public void testPn() {
		LocalDate start = LocalDate.of(2020, 02, 01);
		LocalDate slut = LocalDate.of(2020, 02, 10);
		Pn pn = new Pn(patient, start, slut, 3);
		assertEquals(3, pn.getAntalEnheder(), 0.001);
		assertEquals(LocalDate.of(2020, 02, 01), pn.getStartDen());
		assertEquals(LocalDate.of(2020, 02, 10), pn.getSlutDen());
	}

	@Test
	public void testGivDosis() {
		LocalDate start = LocalDate.of(2020, 02, 01);
		LocalDate slut = LocalDate.of(2020, 02, 10);
		Pn pn = new Pn(patient, start, slut, 3);
		assertEquals(true, pn.givDosis(LocalDate.of(2020, 02, 01)));
		assertEquals(true, pn.givDosis(LocalDate.of(2020, 02, 05)));
		assertEquals(true, pn.givDosis(LocalDate.of(2020, 02, 10)));
		assertEquals(false, pn.givDosis(LocalDate.of(2020, 01, 01)));
		assertEquals(false, pn.givDosis(LocalDate.of(2020, 03, 01)));
	}

	@Test
	public void testAntalDage_sammeDag() {
		LocalDate start = LocalDate.of(2020, 02, 01);
		LocalDate slut = LocalDate.of(2020, 02, 10);
		Pn pn = new Pn(patient, start, slut, 3);
		pn.givDosis(LocalDate.of(2020, 02, 03));
		pn.givDosis(LocalDate.of(2020, 02, 03));
		assertEquals(1, pn.antalDage());
	}

	@Test
	public void testAntalDage_forskelligDage() {
		LocalDate start = LocalDate.of(2020, 02, 01);
		LocalDate slut = LocalDate.of(2020, 02, 15);
		Pn pn = new Pn(patient, start, slut, 3);
		pn.givDosis(LocalDate.of(2020, 02, 03));
		pn.givDosis(LocalDate.of(2020, 02, 10));
		assertEquals(8, pn.antalDage());
	}

	/**
	 * Test at en dosis bliver givet én gang på én dag.
	 */
	@Test
	public void testDoegnDosis_EnGangGivetPaaSammeDag() {
		LocalDate start = LocalDate.of(2020, 02, 01);
		LocalDate slut = LocalDate.of(2020, 02, 20);
		Pn pn = new Pn(patient, start, slut, 3);
		pn.givDosis(LocalDate.of(2020, 02, 01));
		assertEquals(3.0, pn.doegnDosis(), 0.001);
	}

	/**
	 * Test at en dosis bliver givet flere gange på én dag.
	 */
	@Test
	public void testDoegnDosis_flereGangeGivetPaaSammeDag() {
		LocalDate start = LocalDate.of(2020, 02, 01);
		LocalDate slut = LocalDate.of(2020, 02, 20);
		Pn pn = new Pn(patient, start, slut, 3);
		pn.givDosis(LocalDate.of(2020, 02, 01));
		pn.givDosis(LocalDate.of(2020, 02, 01));
		pn.givDosis(LocalDate.of(2020, 02, 01));

		assertEquals(9.0, pn.doegnDosis(), 0.001);
	}

	/**
	 * Test at en dosis bliver givet få gange på mange dage.
	 */
	@Test
	public void testDoegnDosis_FaaGangeGivetPaaMangeDage() {
		LocalDate start = LocalDate.of(2020, 02, 01);
		LocalDate slut = LocalDate.of(2020, 02, 10);
		Pn pn = new Pn(patient, start, slut, 1);

		pn.givDosis(LocalDate.of(2020, 02, 01));
		pn.givDosis(LocalDate.of(2020, 02, 05));
		pn.givDosis(LocalDate.of(2020, 02, 10));

		assertEquals(0.3, pn.doegnDosis(), 0.001);
	}

	/**
	 * Test mange doser givet på én dag.
	 */
	@Test
	public void testSamletDosis_MangeGangePaaEnDag() {
		LocalDate start = LocalDate.of(2020, 02, 01);
		LocalDate slut = LocalDate.of(2020, 02, 10);
		Pn pn = new Pn(patient, start, slut, 3);

		pn.givDosis(LocalDate.of(2020, 02, 01));
		pn.givDosis(LocalDate.of(2020, 02, 01));
		pn.givDosis(LocalDate.of(2020, 02, 01));
		pn.givDosis(LocalDate.of(2020, 02, 01));
		pn.givDosis(LocalDate.of(2020, 02, 01));

		assertEquals(15, pn.samletDosis(), 0.001);
	}

	/**
	 * Test få gange givet over mange dage.
	 */
	@Test
	public void testSamletDosis_FaaGangePaaMangeDage() {
		LocalDate start = LocalDate.of(2020, 02, 01);
		LocalDate slut = LocalDate.of(2020, 02, 10);
		Pn pn = new Pn(patient, start, slut, 3);

		pn.givDosis(LocalDate.of(2020, 02, 01));
		pn.givDosis(LocalDate.of(2020, 02, 10));

		assertEquals(6, pn.samletDosis(), 0.001);
	}

}
