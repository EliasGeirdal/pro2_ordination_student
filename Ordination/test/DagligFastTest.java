package test;

import ordination.DagligFast;
import ordination.Dosis;
import ordination.Patient;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.Assert.*;

public class DagligFastTest {
    Patient patient;
    LocalDate startDato;
    LocalDate slutDato;
    DagligFast dagligFast;

    @org.junit.Before
    public void setUp() throws Exception {
        patient = new Patient("123456-7890", "Fornavn Efternavn", 80.5);
        startDato = LocalDate.of(2020, 02, 01);
        slutDato = LocalDate.of(2020, 02, 10);
        dagligFast = new DagligFast(patient, startDato, slutDato);
    }

    @Test
    public void testDagligFast() {
        assertEquals(LocalDate.of(2020, 02, 01), dagligFast.getStartDen());
        assertEquals(LocalDate.of(2020, 02, 10), dagligFast.getSlutDen());
    }

    @org.junit.Test
    public void testOpretDosis() {
        dagligFast.opretDosis(LocalTime.of(9, 0), 1.0);
        Dosis dosis = dagligFast.getDoser()[0];
        assertEquals(LocalTime.of(9, 0), dosis.getTid());
        assertEquals(1.0, dosis.getAntal(), 0.001);
    }

    @org.junit.Test
    public void testDoegnDosis_TC1() {
        assertEquals(0.0, dagligFast.doegnDosis(), 0.001);
    }

    @org.junit.Test
    public void testDoegnDosis_TC2() {
        dagligFast.opretDosis(LocalTime.of(9, 0), 2);
        assertEquals(2.0, dagligFast.doegnDosis(), 0.001);
    }

    @org.junit.Test
    public void testDoegnDosis_TC3() {
        dagligFast.opretDosis(LocalTime.of(9, 0), 2);
        dagligFast.opretDosis(LocalTime.of(12, 0), 5);
        assertEquals(7.0, dagligFast.doegnDosis(), 0.001);
    }

    @org.junit.Test(expected = IndexOutOfBoundsException.class)
    public void testDoegnDosis_TC4() {
        dagligFast.opretDosis(LocalTime.of(9, 0), 2);
        dagligFast.opretDosis(LocalTime.of(12, 0), 5);
        dagligFast.opretDosis(LocalTime.of(13, 0), 4);
        dagligFast.opretDosis(LocalTime.of(14, 0), 2);
        dagligFast.opretDosis(LocalTime.of(15, 0), 1);
    }



    @org.junit.Test
    public void testSamletDosis_TC1() {
        assertEquals(0.0, dagligFast.samletDosis(), 0.001);
    }

    @org.junit.Test
    public void testSamletDosis_TC2() {
        dagligFast.opretDosis(LocalTime.of(9, 0), 2);
        assertEquals(20.0, dagligFast.samletDosis(), 0.001);
    }

    @org.junit.Test
    public void testSamletDosis_TC3() {
        LocalDate nySlutDato = LocalDate.of(2020, 02, 01);
        DagligFast lokalDagligFast = new DagligFast(patient, startDato, nySlutDato);
        lokalDagligFast.opretDosis(LocalTime.of(9, 0), 2);
        lokalDagligFast.opretDosis(LocalTime.of(12, 0), 5);
        assertEquals(7.0, lokalDagligFast.samletDosis(), 0.001);
    }

}