package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Patient;

public class DagligSkaevTest {
	Patient patient;
	DagligSkaev ds;

	@Before
	public void setUp() throws Exception {
		patient = new Patient("123456-7890", "Fornavn Efternavn", 60.00);
		LocalDate start = LocalDate.of(2020, 9, 14);
		LocalDate slut = LocalDate.of(2020, 9, 15);
		ds = new DagligSkaev(patient, start, slut);
	}

	/**
	 * Test DagligSkaev constructor.
	 */
	@Test
	public void testDagligSkaev() {
		assertEquals(LocalDate.of(2020, 9, 14), ds.getStartDen());
		assertEquals(LocalDate.of(2020, 9, 15), ds.getSlutDen());
	}

	@Test
	public void testOpretDosis() {
		ds.opretDosis(LocalTime.of(9, 00), 1);
		assertEquals(1.00, ds.getDoser().get(0).getAntal(), 0.001);
		assertEquals(LocalTime.of(9, 00), ds.getDoser().get(0).getTid());
	}

	@Test
	public void testDoegnDosis_TC1() {
		assertEquals(0.00, ds.doegnDosis(), 0.001);
	}

	@Test
	public void testDoegnDosis_TC2() {
		ds.opretDosis(LocalTime.of(10, 00), 2);
		assertEquals(2, ds.doegnDosis(), 0.001);
	}

	@Test
	public void testDoegnDosis_TC3() {
		ds.opretDosis(LocalTime.of(8, 00), 1);
		ds.opretDosis(LocalTime.of(14, 00), 3);
		ds.opretDosis(LocalTime.of(17, 00), 4);
		assertEquals(8, ds.doegnDosis(), 0.001);
	}

	@Test
	public void testSamletDosis_TC1() {
		LocalDate start = LocalDate.of(2020, 10, 01);
		LocalDate slut = LocalDate.of(2020, 10, 10);
		DagligSkaev ds_1 = new DagligSkaev(patient, start, slut);

		ds_1.opretDosis(LocalTime.of(8, 00), 1);
		ds_1.opretDosis(LocalTime.of(14, 00), 3);
		ds_1.opretDosis(LocalTime.of(17, 00), 4);
		assertEquals(80, ds_1.samletDosis(), 0.001);
	}

	@Test
	public void testSamletDosis_TC2() {
		LocalDate start = LocalDate.of(2020, 10, 01);
		LocalDate slut = LocalDate.of(2020, 10, 01);
		DagligSkaev ds_1 = new DagligSkaev(patient, start, slut);

		ds_1.opretDosis(LocalTime.of(8, 00), 1);
		ds_1.opretDosis(LocalTime.of(14, 00), 3);
		ds_1.opretDosis(LocalTime.of(17, 00), 4);
		assertEquals(8, ds_1.samletDosis(), 0.001);
	}

	@Test
	public void testSamletDosis_TC3() {
		LocalDate start = LocalDate.of(2020, 10, 01);
		LocalDate slut = LocalDate.of(2020, 10, 10);
		DagligSkaev ds_1 = new DagligSkaev(patient, start, slut);

		assertEquals(0, ds_1.samletDosis(), 0.001);
	}

}
